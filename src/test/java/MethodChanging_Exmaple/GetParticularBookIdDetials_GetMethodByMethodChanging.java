package MethodChanging_Exmaple;

import io.restassured.RestAssured;

public class GetParticularBookIdDetials_GetMethodByMethodChanging {

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		//Build Request
		RestAssured.
		given()
		.baseUri("https://restful-booker.herokuapp.com/")
		.basePath("booking/{id}")
		.pathParam("id", 22)
		//Hit request and get response using the get method
		.when()
		.get()
		.then()
		//validate the response an dispaly on the console
		.statusCode(200)
		.log()
		.all();
	}

}
