package RestAssuredAPIWithMethodChanging;

import io.restassured.RestAssured;
//import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class PostMethod_CreateBooking {

	public static void main(String[] args) 
	{
		//Build Request
		RequestSpecification RestAssuredObject=RestAssured.given();
	
		RestAssuredObject=RestAssuredObject.log().all();
		
		RestAssuredObject.baseUri("https://restful-booker.herokuapp.com/");
		RestAssuredObject.basePath("booking");
		RestAssuredObject.body("{\r\n"
				+ "    \"firstname\" : \"Jim\",\r\n"
				+ "    \"lastname\" : \"Brown\",\r\n"
				+ "    \"totalprice\" : 111,\r\n"
				+ "    \"depositpaid\" : true,\r\n"
				+ "    \"bookingdates\" : {\r\n"
				+ "        \"checkin\" : \"2018-01-01\",\r\n"
				+ "        \"checkout\" : \"2019-01-01\"\r\n"
				+ "    },\r\n"
				+ "    \"additionalneeds\" : \"Breakfast\"\r\n"
				+ "}");
		//RestAssuredObject.contentType("application/json");
		RestAssuredObject.contentType("application/json");
		
		//Hit Request and get response
		
		Response ResponseValue = RestAssuredObject.post();
		//ResponseValue.then().log().body();
		
		//Validation Response
		ValidatableResponse Validate = ResponseValue.then();
		Validate.statusCode(200);
		//Validate.log().status();
		
		
		
	}

}
